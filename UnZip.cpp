/*****************************************************************************
 *
 *	Usage1: Unzip <Zip file name> <Output folder>
 * Usage2: Unzip -I<Zip file name> -O<Output folder>
 * Usage3: Unzip -I<Zip path name> -O<Output folder> [-N]
 *				 -I: Input path name, wild card allow.
 *				 -N: Do not overwrite existing file.
 *
 *****************************************************************************/

#include "stdafx.h"
#include "Unzip.h"
#include "doZip.h"
#include "logs.h"
#include "getopt.h"
#include "prodlib.h"
#include "utils.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////

char  acIniFile[_MAX_PATH], acLogFile[_MAX_PATH], acCntyCode[4];
char	acZipTmpl[_MAX_PATH], acUnzipFolder[_MAX_PATH];
bool  bUnzipAll, bOverWrite;

CWinApp theApp;

using namespace std;

/************************************** Usage ********************************
 *
 *
 *****************************************************************************/

void Usage()
{
   printf("\nUsage: Unzip -I<zip file> -O<unzip folder> [-N] [-L<Log file>]\n");
   exit(1);
}

/********************************** ParseCmd() *******************************
 *
 *
 *****************************************************************************/

void ParseCmd(int argc, char* argv[])
{
   char  chOpt;
   char *pszParam, sTmp[_MAX_PATH];

   bUnzipAll = false;
   bOverWrite=true;
   acLogFile[0] = 0;

   // Make program compatible with old 16-bit DOS app.
   if (argc == 2 && *argv[1] != '-')
   {
      strcpy(acZipTmpl, argv[1]);
      strcpy(sTmp, argv[1]);
      if (pszParam = strrchr(sTmp, '\\'))
      {
         *pszParam = 0;
         strcpy(acUnzipFolder, sTmp);
      } else
      {
         _getcwd(acUnzipFolder, _MAX_PATH);
      }
      return;
   } 
   if (argc == 3 && *argv[1] != '-' && *argv[2] != '-')
   {
      strcpy(acZipTmpl, argv[1]);
      strcpy(acUnzipFolder, argv[2]);
      return;
   }

   while (1)
   {
      chOpt = GetOption(argc, argv, "C:I:O:L:N?", &pszParam);
      if (chOpt > 1)
      {
         // chOpt is valid argument
         switch (chOpt)
         {
            case 'N':   // Not overwrite existing file
               bOverWrite = false;
               break;

            case 'I':   // Input zip file
               if (pszParam != NULL)
                  strcpy(acZipTmpl, pszParam);
               else
               {
                  printf("Missing input zip file\n");
                  Usage();
               }
               break;

            case 'O':   // Output folder
               if (pszParam != NULL)
                  strcpy(acUnzipFolder, pszParam);
               else
               {
                  printf("Missing output folder\n");
                  Usage();
               }
               break;

            case 'C':   // county code
               if (pszParam != NULL)
                  strcpy(acCntyCode, pszParam);
               else
               {
                  printf("Missing county code\n");
                  Usage();
               }
               break;

            case 'L':   // Log file
               if (pszParam != NULL)
                  strcpy(acLogFile, pszParam);
               else
               {
                  printf("Missing log file name.  Output message to screen.\n");
                  Usage();
               }
               break;

            case '?':   // usage info
            default:
               Usage();
               break;
         }
      }
      if (chOpt == 0)
      {
         // end of argument list
         break;
      }
      if ((chOpt == 1) || (chOpt == -1))
      {
         // standalone param or error
         LogMsg("Argument [%s] not recognized\n", pszParam);
         break;
      }
   }

   // CHeck for wild card input
   if (strchr(acZipTmpl, '?') || strchr(acZipTmpl, '*'))
      bUnzipAll = true;
}

/*****************************************************************************
 *
 * Usage:
 *    Unzip -I<zip file> -O<unzip folder> [-A] [-N]
 *
 *****************************************************************************/

int _tmain(int argc, TCHAR* argv[], TCHAR* envp[])
{
   int   iRet;

   // initialize MFC 
   if (!AfxWinInit(::GetModuleHandle(NULL), NULL, ::GetCommandLine(), 0))
   {
      printf("Fatal Error: MFC initialization failed\n");
      return 1;
   }

   CString strAppVer;
   strAppVer.LoadString(APP_VERSION_INFO);
   printf("%s\n", strAppVer);

   if (argc < 2)
      Usage();

   // Parse command line
   ParseCmd(argc, argv);

   char  *pTmp;
   pTmp = GetExePath(acIniFile, argv[0]);
   if (*pTmp)
      strcat(acIniFile, "\\Unzip.ini");
   else
      strcpy(acIniFile, "C:\\Tools\\Unzip.ini");

   if (_access(acIniFile, 0))
   {
      printf("***** ERROR: INI file not found --> %s", acIniFile);
      exit(1);
   }

   if (!acLogFile[0] && !_access(acIniFile, 0))
      GetPrivateProfileString("System", "LogFile", "", acLogFile, _MAX_PATH, acIniFile);
   if (acLogFile[0])
      open_log(acLogFile, "a+");

   LogMsg("%s\n\n", strAppVer);

   if (bUnzipAll)
      iRet = unzipAll(acZipTmpl, acUnzipFolder, bOverWrite);
   else
      iRet = unzipOne(acZipTmpl, acUnzipFolder, bOverWrite);

   close_log();

   exit(iRet);
}

