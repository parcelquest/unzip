/*
   This file contains loging functions
 */

#include "stdafx.h"
#include "logs.h"

static FILE *fdLog = NULL;
static FILE *fdUsecodeLog = NULL;
static char myLogMsg[256], sUseLog[_MAX_PATH];

#define WFL_MAX_TIMEBUF 80
char *TimeStamp()
{
   static char timeStr[WFL_MAX_TIMEBUF];
   time_t t = 0;
   struct tm *pTime  = NULL;
   
   time(&t);
   pTime = localtime(&t);
   strftime( timeStr ,WFL_MAX_TIMEBUF ,"%H:%M:%S %B %d, %Y" ,pTime);
   return timeStr;
}

char *DateStamp()
{
   static char dateStr[WFL_MAX_TIMEBUF];
   time_t t = 0;
   struct tm *pTime  = NULL;
   
   time(&t);
   pTime = localtime(&t);
   strftime(dateStr ,WFL_MAX_TIMEBUF ,"%Y%m%d" ,pTime);
   return dateStr;
}

void open_log(char *pLogFile, char *pMode, char *pFrom)
{
   if (fdLog)
      fclose(fdLog);

   if ((fdLog=fopen(pLogFile, pMode))==NULL) 
   {
      fprintf(stderr ,"error opening log file %s" ,pLogFile);
      exit(1);
   }

   if (pFrom && *pFrom)
      fprintf(fdLog ,"(%s) %s: Open log file %s\n", TimeStamp(), pFrom ,pLogFile);
   else
      fprintf(fdLog ,"(%s) Open log file %s\n" ,TimeStamp(), pLogFile);

   myLogMsg[0] = 0;
}

void close_log()
{
   if (fdLog) 
   {
      fprintf(fdLog, "(%s) Closing log\n\n" ,TimeStamp());
      fflush(fdLog);
      fclose(fdLog);
      fdLog = NULL;
   }
}

/* clone of LOG */
void LOG_ERROR(char *fname, int lineno, char *fmt, ...)
{
   char buffer[8192];
   char * bp = buffer;
   va_list ap;

   va_start(ap ,fmt);
   sprintf(bp, "(%s) %12s :%4d\t" ,TimeStamp() ,fname ,lineno);
   bp += strlen( bp ); 
   vsprintf(bp ,fmt ,ap);
   strcat(bp ,"\n");
   va_end(ap);

   if (!fdLog) fdLog = stdout;
   fprintf( fdLog ,"%s" ,buffer );
}

void LOG(char *fname, int lineno, char *fmt, ...)
{
   char buffer[8192];
   char * bp = buffer;
   va_list ap;

   va_start(ap ,fmt);
   sprintf(bp, "(%s) %12s :%4d\t" ,TimeStamp() ,fname ,lineno);
   bp += strlen( bp ); 
   vsprintf(bp ,fmt ,ap);
   strcat(bp ,"\n");
   va_end(ap);

   if (!fdLog) fdLog = stdout;
   fprintf( fdLog ,"%s" ,buffer );
}

void LogMsg(char *fmt, ...)
{
   char buffer[8192];
   char * bp = buffer;
   va_list ap;

   va_start(ap ,fmt);
   sprintf(bp, "(%s) \t" ,TimeStamp());
   bp += strlen( bp ); 
   vsprintf(bp ,fmt ,ap);
   strcat(bp ,"\n");
   va_end(ap);

   if (fdLog) 
   {
      fprintf(fdLog ,"%s" ,buffer);
      fflush(fdLog);
   } else
      printf("%s" ,buffer);

   if (strstr(buffer, "***"))
   {
      strncpy(myLogMsg, buffer, 255);
      myLogMsg[255] = 0;
   }
}

void LogMsg0(char *fmt, ...)
{
   char buffer[8192];
   char * bp = buffer;
   va_list ap;

   va_start(ap ,fmt);
   vsprintf(bp ,fmt ,ap);
   strcat(bp ,"\n");
   va_end(ap);

   if (fdLog) 
   {
      fprintf(fdLog ,"%s" ,buffer);
      fflush(fdLog);
   } else
      printf("%s" ,buffer);
}

void LogMsg1(char *fmt, ...)
{
   char buffer[8192];
   char * bp = buffer;
   va_list ap;

   va_start(ap ,fmt);
   sprintf(bp, "(%s) \t", DateStamp());
   bp += strlen( bp ); 
   vsprintf(bp ,fmt ,ap);
   strcat(bp ,"\n");
   va_end(ap);

   if (fdLog) 
   {
      fprintf(fdLog ,"%s" ,buffer);
      fflush(fdLog);
   } else
      printf("%s" ,buffer);
}

void LogMsgD(char *fmt, ...)
{
   char buffer[8192];
   char * bp = buffer;
   va_list ap;

   va_start(ap ,fmt);
   vsprintf(bp ,fmt ,ap);
   strcat(bp ,"\n");
   va_end(ap);
   printf("\n%s" ,buffer);

   if (fdLog) 
   {
      fprintf(fdLog ,"%s" ,buffer);
      fflush(fdLog);
   }

   if (strstr(buffer, "***"))
   {
      strncpy(myLogMsg, buffer, 255);
      myLogMsg[255] = 0;
   }
}

char *getLastErrorLog()
{
   return myLogMsg;
}

void logUsecode(char *pUsecode, char *pApn)
{
   char buffer[512];
   char *bp = buffer;

   sprintf(buffer, "%.14s ->%.10s\n", pApn, pUsecode);
   if (fdUsecodeLog) 
   {
      fputs(buffer, fdUsecodeLog);
      fflush(fdUsecodeLog);
   } else if (fdLog)
   {
      fprintf(fdLog ,"%s" ,buffer);
      fflush(fdLog);
   }
}

bool openUsecodeLog(char *pCnty, char *pIniFile)
{
   char sTmp[_MAX_PATH];
   bool bRet = true;

   if (fdUsecodeLog)
      fclose(fdUsecodeLog);

   GetPrivateProfileString("System", "UsecodeLog", "", sTmp, _MAX_PATH, pIniFile);
   if (sTmp[0] <= ' ')
   {
      sprintf(myLogMsg ,"UsecodeLog has not been set in %s file.", pIniFile);
      return false;
   }

   sprintf(sUseLog, sTmp, pCnty, pCnty); 
   if ((fdUsecodeLog=fopen(sUseLog, "w"))==NULL) 
   {
      sprintf(myLogMsg ,"Error opening usecode log file %s", sUseLog);
      bRet = false;
      sUseLog[0] = 0;
   }

   return bRet;
}

void closeUsecodeLog()
{
   if (fdUsecodeLog) 
   {
      fflush(fdUsecodeLog);         

      // If file is empty, delete it
      if (_filelength(fdUsecodeLog->_file) < 1)
      {
         fclose(fdUsecodeLog);
         remove(sUseLog);
      } else
         fclose(fdUsecodeLog);

      fdUsecodeLog = NULL;
   }

}
